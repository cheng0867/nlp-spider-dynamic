package org.wisdomdata.common;

import java.util.List;

import org.wisdomdata.framework.Extractor;

/**
 * 定义一个信息抽取者抽象类
 * 抽取者可以定义子抽取者，不过子抽取者是有顺序的
 * @author Clebeg 
 * @time 2014-12-06
 * */
public abstract class AbstractStringExtractor implements Extractor{
	
	List<SecondExtractor> childExtractors;
	public List<SecondExtractor> getChildExtractors() {
		return childExtractors;
	}
	public void setChildExtractors(List<SecondExtractor> childExtractors) {
		this.childExtractors = childExtractors;
	}

	/**
	 * 抽取的信息的名字，如果名字为空或者NULL那么就以默认值作为名字，
	 * 或者用父抽取器的名字
	 * */
	private String extractName;
	public String getExtractName() {
		return extractName;
	}
	public void setExtractName(String extractName) {
		this.extractName = extractName;
	}
	
	private String extractResult;
	public String getExtractResult() {
		return extractResult;
	}
	public void setExtractResult(String extractResult) {
		this.extractResult = extractResult;
	}
	
	
	public void extract() {
		//尝试三次
		int mark = 0;
		while(true) {
			mark++;
			try {
				if (prepareExtract()) {
					innerExtract();
					quitExtract();
				}
			} catch (Exception e) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				if (3 <= mark) {
					break;
				}
				else 
					continue;
			}	
			break;
		} 
		
	}
	public boolean prepareExtract() {
		return true;
	}

	public void quitExtract() {
		String secondExtrat = this.getExtractResult().trim();
		if (this.getChildExtractors() != null && secondExtrat != null && secondExtrat.equalsIgnoreCase("") == false) {
			
			for (SecondExtractor e : this.getChildExtractors()) {
				e.setSourceText(secondExtrat);
				e.extract();
				secondExtrat = e.getExtractResult().trim();
			}
			this.setExtractResult(secondExtrat);
		}
		
	}
}
