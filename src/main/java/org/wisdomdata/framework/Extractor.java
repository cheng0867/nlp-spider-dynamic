package org.wisdomdata.framework;


public interface Extractor {
	boolean prepareExtract();
	void extract();
	void innerExtract() throws Exception;
	void quitExtract();
}
