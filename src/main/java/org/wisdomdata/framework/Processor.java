package org.wisdomdata.framework;

/**
 * 处理器，一个任务由许多个处理器组成一个处理链，处理器有下面三种类型
 * @author Clebeg
 * @time 2014-12-07
 * @version v1
 * */
public interface Processor {
	
	boolean prepareProcess();
	void process();
	void innerProcess();
	void quitProcess();
}
