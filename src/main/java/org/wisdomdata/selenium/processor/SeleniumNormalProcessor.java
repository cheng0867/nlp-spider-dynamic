package org.wisdomdata.selenium.processor;

import org.wisdomdata.framework.Processor;
import org.wisdomdata.selenium.SeleniumProcessor;

/**
 * desription: 正常处理者，即只做页面抽取工作
 * @author Clebeg 2014-12-09
 * */
public class SeleniumNormalProcessor extends SeleniumProcessor {
	public void innerProcess() {
		this.commonExtract();
		if (this.getChildProcessors() != null && this.getChildProcessors().size() > 0) {
			for (Processor processor : this.getChildProcessors()) {
				processor.process();
			}	
		}
	}
}
