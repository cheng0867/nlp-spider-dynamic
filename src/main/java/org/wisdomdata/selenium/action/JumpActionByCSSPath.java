package org.wisdomdata.selenium.action;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumAction;

public class JumpActionByCSSPath extends SeleniumAction {
	private String csspath;
	

	public String getCsspath() {
		return csspath;
	}


	public void setCsspath(String csspath) {
		this.csspath = csspath;
	}


	public void action() throws NoSuchElementException{
		
		WebElement saveButton = this.getParentSearchContext().findElement(
					By.cssSelector(getCsspath())
				);
		String newUri = saveButton.getAttribute("href");
		((WebDriver) this.getSearchContext()).get(newUri);
	}

}
