package org.wisdomdata.selenium;

import org.openqa.selenium.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.wisdomdata.framework.Action;

public abstract class SeleniumAction implements Action{
	//需要运行抓取的驱动
	
	private SearchContext searchContext;
	
	public SearchContext getSearchContext() {
		return searchContext;
	}
	@Autowired
	public void setSearchContext(SearchContext searchContext) {
		this.searchContext = searchContext;
	}
	
	private String parentUri;
	public String getParentUri() {
		return parentUri;
	}
	public void setParentUri(String parentUri) {
		this.parentUri = parentUri;
	}
	
	private SearchContext parentSearchContext;

	public SearchContext getParentSearchContext() {
		return parentSearchContext;
	}
	public void setParentSearchContext(SearchContext parentSearchContext) {
		this.parentSearchContext = parentSearchContext;
	}
	
}
