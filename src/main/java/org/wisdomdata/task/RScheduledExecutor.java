package org.wisdomdata.task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Required;

public class RScheduledExecutor {
	
	private long delayTime;
	private long intervalTime;
	private char timeUnit = 'S';
	private TimeUnit unit = TimeUnit.SECONDS;
	private TaskCrawler taskCrawler;
	

	public void schedule() {
		ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
		service.scheduleWithFixedDelay(this.getTaskCrawler(), this.getDelayTime(), this.getIntervalTime(), this.unit);
	}

	public long getDelayTime() {
		return delayTime;
	}
	@Required
	public void setDelayTime(long delayTime) {
		this.delayTime = delayTime;
	}

	public long getIntervalTime() {
		return intervalTime;
	}
	@Required
	public void setIntervalTime(long intervalTime) {
		this.intervalTime = intervalTime;
	}

	public char getTimeUnit() {
		return timeUnit;
	}

	@Required
	public void setTimeUnit(char timeUnit) {
		this.timeUnit = timeUnit;
		switch (this.timeUnit) {
			case 'S':
				this.unit = TimeUnit.SECONDS;
				break;
			case 's':
				this.unit = TimeUnit.SECONDS;
				break;
			case 'M':
				this.unit = TimeUnit.MINUTES;
				break;
			case 'm':
				this.unit = TimeUnit.MINUTES;
				break;
			case 'H':
				this.unit = TimeUnit.HOURS;
				break;
			case 'h':
				this.unit = TimeUnit.HOURS;
				break;
			case 'D':
				this.unit = TimeUnit.DAYS;
				break;
			case 'd':
				this.unit = TimeUnit.DAYS;
				break;
			default:
				break;
		}
	}

	public TaskCrawler getTaskCrawler() {
		return taskCrawler;
	}

	public void setTaskCrawler(TaskCrawler taskCrawler) {
		this.taskCrawler = taskCrawler;
	}

	
	
}
